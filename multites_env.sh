#!/bin/bash

export DJANGO_DEBUG=False
export DJANGO_TEMPLATE_DEBUG=False
export DJANGO_SITE_ROOT=/home/davide/Documenti/multites
export DJANGO_DATABASES_ENGINE=django.db.backends.postgresql_psycopg2
export DJANGO_DATABASES_NAME=multites
export DJANGO_DATABASES_USER=multites
export DJANGO_DATABASES_PASSWORD=multites
export DJANGO_DATABASES_HOST=localhost
export DJANGO_DATABASES_PORT=
export DJANGO_ALLOWED_HOSTS=
export DJANGO_MEDIA_ROOT=/home/davide/Documenti/media
export DJANGO_STATIC_ROOT=/home/davide/Documenti/static
export DJANGO_SECRET_KEY=7nhbn=qa3jfkcz\&m3f8kt\^\(13\#_n\%n\*h\%xnkcm6e\!w9mkgqqg7
export DJANGO_WHOOSH_INDEX=/home/davide/Documenti/whoosh

if [ ! -d "$DJANGO_WHOOSH_INDEX" ]; then
  mkdir $DJANGO_WHOOSH_INDEX
fi

if [ ! -d "$DJANGO_MEDIA_ROOT" ]; then
  mkdir $DJANGO_MEDIA_ROOT
fi

if [ ! -d "$DJANGO_STATIC_ROOT" ]; then
  mkdir $DJANGO_STATIC_ROOT
fi
