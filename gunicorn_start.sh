#!/bin/bash
 
NAME="multites_app"                                  	# Name of the application
DJANGODIR=/home/multites/webapps/multites_env/multites  # Django project directory
SOCKFILE=/home/multites/webapps/multites_env/run/gunicorn.sock  # we will communicte using this unix socket
USER=webappsmultites                                  	# the user to run as
GROUP=webapps                                     	# the group to run as
NUM_WORKERS=3                                     	# how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=multites.settings             	# which settings file should Django use
DJANGO_WSGI_MODULE=multites.wsgi                     	# WSGI module name
 
echo "Starting $NAME as `whoami`"
 
# Activate the virtual environment
cd $DJANGODIR
source ../bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH
 
# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

export DJANGO_DEBUG=False
export DJANGO_TEMPLATE_DEBUG=False
export DJANGO_SITE_ROOT=/home/multites/webapps/multites_env/multites
export DJANGO_DATABASES_ENGINE=django.db.backends.postgresql_psycopg2
export DJANGO_DATABASES_NAME=multites
export DJANGO_DATABASES_USER=multites
export DJANGO_DATABASES_PASSWORD=multites
export DJANGO_DATABASES_HOST=localhost
export DJANGO_DATABASES_PORT=
export DJANGO_ALLOWED_HOSTS=
export DJANGO_MEDIA_ROOT=/home/multites/webapps/multites_env/media
export DJANGO_STATIC_ROOT=/home/multites/webapps/multites_env/static
export DJANGO_SECRET_KEY=7nhbn=qa3jfkcz\&m3f8kt\^\(13\#_n\%n\*h\%xnkcm6e\!w9mkgqqg7
export DJANGO_WHOOSH_INDEX=/home/multites/webapps/multites_env/whoosh

if [ ! -d "$DJANGO_WHOOSH_INDEX" ]; then
  mkdir $DJANGO_WHOOSH_INDEX
fi

if [ ! -d "$DJANGO_MEDIA_ROOT" ]; then
  mkdir $DJANGO_MEDIA_ROOT
fi

if [ ! -d "$DJANGO_STATIC_ROOT" ]; then
  mkdir $DJANGO_STATIC_ROOT
fi
 
# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec ../bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER --group=$GROUP \
  --log-level=debug \
  --bind=unix:$SOCKFILE
