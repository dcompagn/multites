# -*- coding: utf-8 -*-

from django.utils.translation import pgettext, ugettext_lazy as _, ugettext
from django.contrib.auth.forms import AuthenticationForm
import django.forms as forms

class LoginForm(AuthenticationForm):
        
    remember = forms.BooleanField(
        label=_('Ricorda'),
        required=False,                        
    )
    