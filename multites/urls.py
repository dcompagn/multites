# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

import autocomplete_light
# import every app/autocomplete_light_registry.py
autocomplete_light.autodiscover()

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from multites.forms import LoginForm

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'multites.views.home', name='home'),
    # url(r'^multites/', include('multites.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:    
    url(r'^erp/', include('erp.urls')),
    url(r'^login/', 'django.contrib.auth.views.login', {'authentication_form': LoginForm,}, name='views.login'),
    url(r'^logout/', 'django.contrib.auth.views.logout', {'next_page': '/login', }, name='views.logout'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^autocomplete/', include('autocomplete_light.urls')),    
) 

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
