from erp.models import Work, WorkStatusTransition, PhoneNumber

# works = Work.objects.all()
# 
# for w in works:
#     print w
#     transitions = WorkStatusTransition.objects.filter(related_work=w.id)
#     current_status = None
#     for t in transitions:
#         if current_status is None:
#             current_status = t
#         elif t.status > current_status.status:
#             current_status = t
#         else:
#             pass
#     w.current_status = current_status
#     w.save()
    
wst = WorkStatusTransition.objects.all()
for w in wst:
    w.updated = w.related_work.updated
    w.save()

pn = PhoneNumber.objects.all()
for p in pn:
    p.updated = p.related_contact.updated
    p.save()
