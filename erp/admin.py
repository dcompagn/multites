from django.contrib import admin
from erp.models import Call, City, Prov, Contact, Document, PhoneNumber, Work, WorkStatusTransition, Note, UserExtModel
#from django.contrib.admin.templatetags.admin_list import date_hierarchy

#admin.site.register(Phone_Number)
class PhoneNumberInline(admin.TabularInline):
    model = PhoneNumber
    extra = 1

admin.site.register(Call)
admin.site.register(City)
admin.site.register(Prov)

class ContactAdmin(admin.ModelAdmin):
    fields = ['first_name', 'last_name', 'email','company','tags']
    inlines = [PhoneNumberInline]
    search_fields = ['last_name','first_name']
    
admin.site.register(Contact, ContactAdmin)

admin.site.register(Document)
class DocumentInline(admin.StackedInline):
    model = Document
    extra = 1

#admin.site.register(Transition)
#admin.site.register(WorkStatusTransition)
class WorkStatusTransitionInline(admin.TabularInline):
    model = WorkStatusTransition
    extra = 1

class WorkAdmin(admin.ModelAdmin):
    inlines = [DocumentInline, WorkStatusTransitionInline]
    search_fields = ['description',
                     'contacts__first_name', 
                     'contacts__first_name', 
                     'contacts__company']
    
admin.site.register(Work, WorkAdmin)

admin.site.register(Note)

admin.site.register(UserExtModel)
