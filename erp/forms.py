# -*- coding: utf-8 -*-

from django.utils.translation import pgettext, ugettext_lazy as _, ugettext
from django.forms import ModelForm, TypedChoiceField, Textarea, TextInput
from django.forms.models import inlineformset_factory, BaseInlineFormSet
from django.forms.widgets import HiddenInput
from django.forms import ValidationError
from multites.settings import HIDING_STRING

from haystack.forms import SearchForm

import autocomplete_light

from erp.models import Contact, PhoneNumber, Call, Document, Work, WorkStatusTransition, Note

# class TagField(AutoModelSelect2TagField):
#     queryset = Tag.objects
#     search_fields = ['name__icontains', ]
#     def get_model_field_values(self, value):
#         return {'name': value}

class ContactForm(autocomplete_light.ModelForm):
    
    class Meta:
        model = Contact
        fields = '__all__'
        
    def __init__(self, *args, **kwargs):
        hide_condition = kwargs.pop(HIDING_STRING, None)
        super(ContactForm, self).__init__(*args, **kwargs)        
        if hide_condition:
            for field in hide_condition:
                if field in self.fields:
                    self.fields[field].widget = HiddenInput()

class CallForm(autocomplete_light.ModelForm):
    
    class Meta:
        model = Call
        fields = '__all__'
        
    def __init__(self, *args, **kwargs):
        hide_condition = kwargs.pop(HIDING_STRING, None)
        super(CallForm, self).__init__(*args, **kwargs)        
        if hide_condition:
            for field in hide_condition:
                if field in self.fields:
                    self.fields[field].widget = HiddenInput()

class DocumentForm(autocomplete_light.ModelForm):
    
    class Meta:
        model = Document
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        hide_condition = kwargs.pop(HIDING_STRING, None)
        super(DocumentForm, self).__init__(*args, **kwargs)        
        if hide_condition:
            for field in hide_condition:
                if field in self.fields:
                    self.fields[field].widget = HiddenInput()
            
class WorkForm(autocomplete_light.ModelForm):
    
    class Meta:
        model = Work
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        hide_condition = kwargs.pop(HIDING_STRING, None)
        super(WorkForm, self).__init__(*args, **kwargs)        
        if hide_condition:
            for field in hide_condition:
                if field in self.fields:
                    self.fields[field].widget = HiddenInput()

class NoteForm(autocomplete_light.ModelForm): 
    
    class Meta:
        model = Note
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        hide_condition = kwargs.pop(HIDING_STRING, None)
        super(NoteForm, self).__init__(*args, **kwargs)        
        if hide_condition:
            for field in hide_condition:
                if field in self.fields:
                    self.fields[field].widget = HiddenInput()

PhoneNumberFormSet = inlineformset_factory(Contact, 
                                           PhoneNumber,
                                           widgets={'number': TextInput(attrs={'size': 80})
                                            },
                                           extra=1, 
                                           can_delete=True)

class OrderedBaseInlineFormSet(BaseInlineFormSet):
    
    #def get_queryset(self):
    #    qs = super(OrderedBaseInlineFormSet, self).get_queryset()
    #    return qs.order_by('-status', '-date')
    
    def clean(self):
        """Checks that there are no equal states"""
        self.validate_unique()
        if any(self.errors):
            # Don't bother validating the formset unless each form is valid on its own
            return
        statuses = []
        for form in self.forms:
            if 'status' in form.cleaned_data:
                s = {'status' : form.cleaned_data['status'], 
                     'date' : form.cleaned_data['date']}
                if s in statuses:
                    raise ValidationError("Stai aggiungendo due stati uguali nella stessa data")
                statuses.append(s)

WorkStatusTransitionFormSet = inlineformset_factory(Work, 
                                                    WorkStatusTransition,
                                                    formset=OrderedBaseInlineFormSet,
                                                    widgets={'note': Textarea(attrs={'cols': 60, 'rows': 1}), 
                                                             #'date': DateInput(initial=datetime.date.today),
                                                             }, 
                                                    extra=1, 
                                                    can_delete=True,
                                                    )

class CustomSearchForm(SearchForm):    
    def __init__(self, *args, **kwargs):
        super(CustomSearchForm, self).__init__(*args, **kwargs)
        self.fields['q'].widget.attrs.update({'class' : 'form-control'})
        