# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

from haystack.views import SearchView, search_view_factory
from haystack.query import SearchQuerySet

from erp import views, models, forms
from multites.settings import FILTER_PARAM

urlpatterns = patterns('',
    url(r'^$', views.IndexView.as_view(), name='views.index'),
    #url(r'^search/', include('haystack.urls')),
    url(r'^search/', search_view_factory(view_class=views.CustomSearchView, form_class=forms.CustomSearchForm), name='haystack_search'),
    url(r'^contact/$', views.ContactList.as_view(), name='views.contact'),
    url(r'^contact/create/$', views.ContactCreate.as_view(), name='views.contact.create'),
    url(r'^contact/(?P<pk>\d+)/$', views.ContactUpdate.as_view(), name='views.contact.update'),
    url(r'^contact/(?P<pk>\d+)/delete/$', views.ContactDelete.as_view(), name='views.contact.delete'),
    url(r'^call/$', views.CallList.as_view(), name='views.call'),
    url(r'^call/contact/(?P<pk>\d+)/$', views.CallList.as_view(), {FILTER_PARAM: models.Contact.__name__}, name='views.call.contact'),
    url(r'^call/work/(?P<pk>\d+)/$', views.CallList.as_view(), {FILTER_PARAM : models.Work.__name__}, name='views.call.work'),
    url(r'^call/create/$', views.CallCreate.as_view(), name='views.call.create'),
    url(r'^call/create/contact/(?P<pk>\d+)/$', views.CallCreate.as_view(), {FILTER_PARAM: models.Contact.__name__}, name='views.call.create.contact'),
    url(r'^call/create/work/(?P<pk>\d+)/$', views.CallCreate.as_view(), {FILTER_PARAM: models.Work.__name__}, name='views.call.create.work'),
    url(r'^call/(?P<pk>\d+)/$', views.CallUpdate.as_view(), name='views.call.update'),
    url(r'^call/(?P<pk>\d+)/delete/$', views.CallDelete.as_view(), name='views.call.delete'),
    url(r'^document/$', views.DocumentList.as_view(), name='views.document'),
    url(r'^document/contact/(?P<pk>\d+)/$', views.DocumentList.as_view(), {FILTER_PARAM: models.Contact.__name__}, name='views.document.contact'),
    url(r'^document/work/(?P<pk>\d+)/$', views.DocumentList.as_view(), {FILTER_PARAM : models.Work.__name__}, name='views.document.work'),
    url(r'^document/create/$', views.DocumentCreate.as_view(), name='views.document.create'),
    url(r'^document/create/contact/(?P<pk>\d+)/$', views.DocumentCreate.as_view(), {FILTER_PARAM: models.Contact.__name__}, name='views.document.create.contact'),
    url(r'^document/create/work/(?P<pk>\d+)/$', views.DocumentCreate.as_view(), {FILTER_PARAM: models.Work.__name__}, name='views.document.create.work'),
    url(r'^document/(?P<pk>\d+)/$', views.DocumentUpdate.as_view(), name='views.document.update'),
    url(r'^document/(?P<pk>\d+)/delete/$', views.DocumentDelete.as_view(), name='views.document.delete'),
    url(r'^work/$', views.WorkList.as_view(), name='views.work'),
    url(r'^work/contact/(?P<pk>\d+)/$', views.WorkList.as_view(), {FILTER_PARAM: models.Contact.__name__}, name='views.work.contact'),
    url(r'^work/create/$', views.WorkCreate.as_view(), name='views.work.create'),
    url(r'^work/create/contact/(?P<pk>\d+)/$', views.WorkCreate.as_view(), {FILTER_PARAM: models.Contact.__name__}, name='views.work.create.contact'),
    url(r'^work/(?P<pk>\d+)/$', views.WorkUpdate.as_view(), name='views.work.update'),
    url(r'^work/(?P<pk>\d+)/delete/$', views.WorkDelete.as_view(), name='views.work.delete'),
    url(r'^note/$', views.NoteList.as_view(), name='views.note'),
    url(r'^note/contact/(?P<pk>\d+)/$', views.NoteList.as_view(), {FILTER_PARAM: models.Contact.__name__}, name='views.note.contact'),
    url(r'^note/work/(?P<pk>\d+)/$', views.NoteList.as_view(), {FILTER_PARAM : models.Work.__name__}, name='views.note.work'),
    url(r'^note/call/(?P<pk>\d+)/$', views.NoteList.as_view(), {FILTER_PARAM : models.Call.__name__}, name='views.note.call'),
    url(r'^note/create/$', views.NoteCreate.as_view(), name='views.note.create'),
    url(r'^note/create/contact/(?P<pk>\d+)/$', views.NoteCreate.as_view(), {FILTER_PARAM: models.Contact.__name__}, name='views.note.create.contact'),
    url(r'^note/create/work/(?P<pk>\d+)/$', views.NoteCreate.as_view(), {FILTER_PARAM: models.Work.__name__}, name='views.note.create.work'),
    url(r'^note/create/call/(?P<pk>\d+)/$', views.NoteCreate.as_view(), {FILTER_PARAM: models.Call.__name__}, name='views.note.create.call'),
    #url(r'^note/create/document/(?P<pk>\d+)/$', views.NoteCreate.as_view(), {FILTER_PARAM: models.Document.__name__}, name='views.note.create.document'),
    url(r'^note/(?P<pk>\d+)/$', views.NoteUpdate.as_view(), name='views.note.update'),
    url(r'^note/(?P<pk>\d+)/delete/$', views.NoteDelete.as_view(), name='views.note.delete'),
    #url(r'^json/contact$', views.ContactListJson.as_view(), name="contact_list_json"),
)

