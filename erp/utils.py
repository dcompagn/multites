import datetime
from django.template.defaultfilters import timesince
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from multites.settings import DATE_FORMAT

#TODO mark_save "from django.utils.safestring import mark_safe"

class CustomMessageMixin(SuccessMessageMixin):
    
    fail_message = 'Impossibile salvare. Controlla i dati inseriti.'
    
    def form_invalid(self, form):
        response = super(CustomMessageMixin, self).form_invalid(form)
        messages.warning(self.request, self.fail_message)
        return response

def bootstrap_label_default(string):
    return u'<span class="label label-default">{0}</span>'.format(string)

def bootstrap_label_primary(string):
    return u'<span class="label label-primary">{0}</span>'.format(string)

def bootstrap_label_success(string):
    return u'<span class="label label-success">{0}</span>'.format(string)

def bootstrap_label_info(string):
    return u'<span class="label label-info">{0}</span>'.format(string)

def bootstrap_label_warning(string):
    return u'<span class="label label-warning">{0}</span>'.format(string)

def bootstrap_label_danger(string):
    return u'<span class="label label-danger">{0}</span>'.format(string)

def bootstrap_tooltip(text, value):
    return u'<a class="tip" data-toggle="tooltip" data-placement="right" title="{1}">{0}</a>'.format(text, value)

def bootstrap_tooltip_button(text, value):
    return u'<a class="tip btn btn-default btn-xs" data-toggle="tooltip" data-placement="right" data-trigger="click" title="{1}" role="button">{0}</a>'.format(text, value)

def bootstrap_user_label(user):
    if user.userextmodel:
        return u'<span class="label" style="background-color: #{0}"><span class="glyphicon glyphicon-pencil"></span>{1}</span>'.format(user.userextmodel.color, user)
    else:
        return bootstrap_label_default(u'<span class="glyphicon glyphicon-pencil"></span>{0}</span> '.format(user))

def bootstrap_tag_label(tags):
    return u' '.join([bootstrap_label_primary(tag.name) for tag in tags])

def bootstrap_date_field(date):
    return date.strftime(DATE_FORMAT)

def bootstrap_date_since_field(date):
    if isinstance(date, datetime.datetime):
        delta = datetime.datetime.now().date() - date.date()
    else:
        delta = datetime.datetime.now().date() - date
    if delta.days < 1:
        string = 'oggi'
    elif delta.days < 2:
        string = 'ieri'
    else:
        string = timesince(date).encode('utf-8').split(',')[0]
    #string = timesince(datetime.datetime.combine(date, datetime.datetime.time(datetime.datetime.now()))).encode('utf-8').split(',')[0]
    return '{0} <small>{1}</small>'.format(bootstrap_date_field(date), string)