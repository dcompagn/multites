# -*- coding: utf-8 -*-

import autocomplete_light
from models import Work, Contact, Call, City, Prov
from taggit.models import Tag

class AutocompleteCity(autocomplete_light.AutocompleteModelBase):
    #autocomplete_js_attributes={'placeholder': 'region name ..'}
    limit_choices = 100
    
    def choices_for_request(self):
        q = self.request.GET.get('q', '')
        prov = self.request.GET.get('prov', None)
        
        if prov:
            choices = self.choices.filter(prov=prov)
        else:
            choices = self.choices.all()
        if q:
            choices = choices.filter(name__icontains=q)

        return self.order_choices(choices)[0:self.limit_choices]

autocomplete_light.register(City, AutocompleteCity)

autocomplete_light.register(Prov, 
    search_fields=['prov',                   
                   'prov_abb',
                   ]
)

autocomplete_light.register(Call, 
    search_fields=['subject', 
                   'type', 
                   'date',
                   #'tags',
                   ]
)

autocomplete_light.register(Contact, 
    search_fields=['first_name', 
                   'last_name', 
                   'company', 
                   'email',
                   #'tags',
                   ]
)
autocomplete_light.register(Work, 
    search_fields=['description',
                   #'tags',
                   ]
)

autocomplete_light.register(Tag)