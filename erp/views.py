# -*- coding: utf-8 -*-

# Create your views here.
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.core.urlresolvers import reverse_lazy
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
#from operator import attrgetter

from datatableview.views import DatatableView
from datatableview.helpers import link_to_model, attrgetter, itemgetter, format_date 
from datatableview.utils import get_datatable_structure

from haystack.views import SearchView
from haystack.query import SearchQuerySet

from erp.models import Contact, Call, Document, Work, Note
from erp.forms import ContactForm, PhoneNumberFormSet, CallForm, DocumentForm, WorkForm, WorkStatusTransitionFormSet, NoteForm, CustomSearchForm
from multites.settings import FILTER_PARAM

import erp.utils as utils

class FilteringMixin(object):
    
    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(FilteringMixin, self).get_form_kwargs()
        if FILTER_PARAM not in self.kwargs.keys():
            return kwargs
        else:
            filter_by = self.kwargs[FILTER_PARAM]
#             if filter_by == Contact.__name__:
#                 kwargs.update({HIDING_STRING: ['related_contact', 'related_work', 'related_call', ] })
#             elif filter_by == Work.__name__:
#                 kwargs.update({HIDING_STRING: ['related_work', 'related_contact', 'related_call', ] })
#             elif filter_by == Call.__name__:
#                 kwargs.update({HIDING_STRING: ['related_call', 'related_contact', 'related_work', ] })
#             elif filter_by == Document.__name__:
#                 kwargs.update({HIDING_STRING: ['related_document', ] })
#             elif filter_by == Note.__name__:
#                 kwargs.update({HIDING_STRING: ['related_note', ] })
#             else:
#                 pass
            return kwargs
    #FIX work__related_contacts not set
    def get_filtered_initial(self, initials):
        if FILTER_PARAM not in self.kwargs.keys():
            return initials
        else:
            filter_by = self.kwargs[FILTER_PARAM]
            try:
                if filter_by == Contact.__name__:
                    instance = Contact.objects.get(id=self.kwargs['pk'])
                    initials['related_contact'] = instance
                elif filter_by == Work.__name__:
                    instance = Work.objects.get(id=self.kwargs['pk'])
                    initials['related_work'] = instance
                elif filter_by == Call.__name__:
                    instance = Call.objects.get(id=self.kwargs['pk'])
                    initials['related_call'] = instance
                    if instance.related_contact:
                        initials['related_contact'] = instance.related_contact
                    if instance.related_work:
                        initials['related_work'] = instance.related_work
                elif filter_by == Document.__name__:
                    instance = Document.objects.get(id=self.kwargs['pk'])
                    initials['related_document'] = instance
                elif filter_by == Note.__name__:
                    instance = Note.objects.get(id=self.kwargs['pk'])
                    initials['related_note'] = instance
                else:
                    pass
            except ObjectDoesNotExist:
                pass 
            return initials
    
    def get_filtered_queryset(self):
        if FILTER_PARAM not in self.kwargs.keys():
            return self.model.objects.all()
        else:
            filter_by = self.kwargs[FILTER_PARAM]
            qs = self.model.objects.none()
            try:
                if filter_by == Contact.__name__:
                    instance = Contact.objects.get(id=self.kwargs['pk'])
                    qs = self.model.objects.filter(related_contact=instance)
                elif filter_by == Work.__name__:
                    instance = Work.objects.get(id=self.kwargs['pk'])
                    qs = self.model.objects.filter(related_work=instance)
                elif filter_by == Call.__name__:
                    instance = Call.objects.get(id=self.kwargs['pk'])
                    qs = self.model.objects.filter(related_call=instance)
                elif filter_by == Document.__name__:
                    instance = Document.objects.get(id=self.kwargs['pk'])
                    qs = self.model.objects.filter(related_document=instance)
                elif filter_by == Note.__name__:
                    instance = Note.objects.get(id=self.kwargs['pk'])
                    qs = self.model.objects.filter(related_note=instance)
                else:
                    pass
            except ObjectDoesNotExist:
                pass
            return qs
    
    def get_filtered_datatable_options(self):
        if FILTER_PARAM not in self.kwargs.keys():
            return self.datatable_options
        if self.kwargs[FILTER_PARAM] == Contact.__name__:
            return self.datatable_contact_options
        if self.kwargs[FILTER_PARAM] == Work.__name__:
            return self.datatable_work_options
        if self.kwargs[FILTER_PARAM] == Document.__name__:
            return self.datatable_document_options
        if self.kwargs[FILTER_PARAM] == Call.__name__:
            return self.datatable_call_options
        if self.kwargs[FILTER_PARAM] == Note.__name__:
            return self.datatable_note_options
        return self.datatable_options
    
class BaseMixin(object):
    
    def form_valid(self, form, *args, **kwargs):
        if self.object is None:
            form.instance.created_by = self.request.user
        form.instance.updated_by = self.request.user        
        return super(BaseMixin, self).form_valid(form, *args, **kwargs)
    
    def form_invalid(self, form, *args, **kwargs):
        return super(BaseMixin, self).form_invalid(form, *args, **kwargs)

class AuthMixin(object):
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(AuthMixin, self).dispatch(*args, **kwargs)


class IndexView(AuthMixin, FormView):
    template_name = 'erp/index.html'
    form_class = CustomSearchForm

#-------------------Contact--------------------#

class ContactList(FilteringMixin, AuthMixin, DatatableView):
    template_name = "erp/contact_list.html"
    model = Contact
    datatable_options = {
        'columns': [('Nominativo', ['last_name', 'first_name']), 
                    'company',
                    'email',
                    ('Tag', ['tags__name'], 'get_tags'),
                    ('Info', 'updated_by__username', 'get_updated_by'),
                    ],
        'structure_template' : 'erp/table_structure.html'
    }
    
    def get_tags(self, instance, *args, **kwargs):
        tags = [tag for tag in instance.tags.all().order_by('name')]
        return utils.bootstrap_tag_label(tags)
    
    def get_updated_by(self, instance, *args, **kwargs):
        return utils.bootstrap_user_label(instance.updated_by)
    
    def get_column_Nominativo_data(self, instance, *args, **kwargs):
        if instance.last_name:
            return link_to_model(instance)
        else:
            return ''
        
    def get_column_email_data(self, instance, *args, **kwargs):
        if instance.email:
            return u'<a href="mailto:{0}" target="_top">{0}</a>'.format(unicode(instance.email))
        else:
            return ''
    
    def get_column_company_data(self, instance, *args, **kwargs):
        if instance.company:
            return link_to_model(instance, text=instance.company)
        else:
            return ''
    
    def get_queryset(self):
        return self.get_filtered_queryset()

class ContactMixin(object):
    
    success_message = 'Contatto salvato correttamente'
    fail_message = 'Impossibile salvare il Contatto. Controlla i dati inseriti.'

    def get_success_message(self, cleaned_data):
        return ContactMixin.success_message % cleaned_data
    
    def form_valid(self, form, phoneNumber_formset):
        self.object = form.save()
        phoneNumber_formset.instance = self.object
        phoneNumber_formset.save() 
        response = HttpResponseRedirect(self.get_success_url())
        success_message = self.get_success_message(form.cleaned_data)
        if success_message:
            messages.success(self.request, success_message)
        return response
    
    def form_invalid(self, form, phoneNumber_formset):
        response = self.render_to_response(
            self.get_context_data(form=form,
                                  phoneNumber_formset=phoneNumber_formset))
        messages.warning(self.request, self.fail_message)
        return response

class ContactCreate(AuthMixin, FilteringMixin, BaseMixin, ContactMixin, CreateView):
    model = Contact
    form_class = ContactForm
    
    def get(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form(self.get_form_class())
        phoneNumber_formset = PhoneNumberFormSet(prefix='phoneNumber')
        return self.render_to_response(
            self.get_context_data(form=form,
                                  phoneNumber_formset=phoneNumber_formset))        
        
    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form(self.get_form_class())
        phoneNumber_formset = PhoneNumberFormSet(self.request.POST, prefix='phoneNumber')
        if (form.is_valid() and phoneNumber_formset.is_valid()):
            return self.form_valid(form, phoneNumber_formset)
        else:
            return self.form_invalid(form, phoneNumber_formset)
    
class ContactUpdate(AuthMixin, FilteringMixin, BaseMixin, ContactMixin, UpdateView):
    model = Contact
    form_class = ContactForm
    
    def get_context_data(self, **kwargs):
        context = super(ContactUpdate, self).get_context_data(**kwargs)
        document_options = DocumentList.datatable_contact_options.copy()
        document_ajax_url = reverse('views.document.contact', args=[str(self.object.id)])        
        context['document_datatable'] = get_datatable_structure(document_ajax_url, document_options, Document)
        work_options = WorkList.datatable_contact_options.copy()
        work_ajax_url = reverse('views.work.contact', args=[str(self.object.id)])
        context['work_datatable'] = get_datatable_structure(work_ajax_url, work_options, Work)
        call_options = CallList.datatable_contact_options.copy()
        call_ajax_url = reverse('views.call.contact', args=[str(self.object.id)])                
        context['call_datatable'] = get_datatable_structure(call_ajax_url, call_options, Call)
        note_options = NoteList.datatable_contact_options.copy()
        note_ajax_url = reverse('views.note.contact', args=[str(self.object.id)])
        context['note_datatable'] = get_datatable_structure(note_ajax_url, note_options, Note)
        return context
    
    def get(self, request, *args, **kwargs):
        
        self.object = self.get_object()
        form = self.get_form(self.get_form_class())
        phoneNumber_formset = PhoneNumberFormSet(instance=self.object, prefix='phoneNumber')
        return self.render_to_response(
            self.get_context_data(form=form,
                                  phoneNumber_formset=phoneNumber_formset))        
        
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form(self.get_form_class())
        phoneNumber_formset = PhoneNumberFormSet(self.request.POST, instance=self.object, prefix='phoneNumber')
        if (form.is_valid() and phoneNumber_formset.is_valid()):
            return self.form_valid(form, phoneNumber_formset)
        else:
            return self.form_invalid(form, phoneNumber_formset)
        
class ContactDelete(AuthMixin, DeleteView):
    model = Contact
    success_url = reverse_lazy('views.contact')
    
#-------------------Call--------------------#

class CallList(FilteringMixin, AuthMixin, DatatableView):
    template_name = "erp/call_list.html"
    model = Call
    datatable_options = {
        'columns': ['subject',                                                        
                    'type', 
                    'date',
                    ('Pratica', ['related_work__description']),
                    ('Contatto', ['related_contact__last_name', 'related_contact__first_name']),
                    ('Tag', ['tags__name'], 'get_tags'),
                    ('Info', 'updated_by__username', 'get_updated_by'), 
                    ],        
        'structure_template' : 'erp/table_structure.html'
    }
    
    datatable_contact_options = {
        'columns': ['subject',                       
                    'type', 
                    'date',
                    ('Pratica', ['related_work__description']),
                    ('Tag', ['tags__name'], 'get_tags'),
                    ('Info', 'updated_by__username', 'get_updated_by'), 
                    ],        
        'structure_template' : 'erp/table_structure_noSearch.html'
    }
    
    datatable_work_options = {
        'columns': ['subject',   
                    'type', 
                    'date',
                    ('Contatto', ['related_contact__last_name', 'related_contact__first_name']),
                    ('Tag', ['tags__name'], 'get_tags'),
                    ('Info', 'updated_by__username', 'get_updated_by'),
                    ],        
        'structure_template' : 'erp/table_structure_noSearch.html'
    }
    
    def get_tags(self, instance, *args, **kwargs):
        tags = [tag for tag in instance.tags.all().order_by('name')]
        return utils.bootstrap_tag_label(tags)
    
    def get_updated_by(self, instance, *args, **kwargs):
        return utils.bootstrap_user_label(instance.updated_by)
    
    def get_column_type_data(self, instance, *args, **kwargs):
        return dict(Call.CALL_TYPES)[instance.type]
    
    def get_column_date_data(self, instance, *args, **kwargs):
        #return instance.date.strftime(DATE_FORMAT)
        return (utils.bootstrap_date_since_field(instance.date), instance.date)
    
    def get_column_Contatto_data(self, instance, *args, **kwargs):
        if instance.related_contact is None:
            return ''
        else:
            return link_to_model(instance.related_contact, text=instance.related_contact)
    
    def get_column_Pratica_data(self, instance, *args, **kwargs):
        if instance.related_work is None:
            return ''
        else:
            return link_to_model(instance.related_work, text=instance.related_work)
    
    def get_column_subject_data(self, instance, *args, **kwargs):
        return link_to_model(instance)
    
    def get_queryset(self):
        return self.get_filtered_queryset()
    
    def get_datatable_options(self):
        return self.get_filtered_datatable_options()

class CallMixin(object):
    
    success_message = 'Chiamata salvata correttamente'
    
    def form_valid(self, form):
        if self.object is None:
            return super(CallMixin, self).form_valid(form)
        else:
#             prev_contact = None
#             prev_work = None
#             if self.object:
#                 prev_contact = self.object.related_contact
#                 prev_work = self.object.related_work
            response = super(CallMixin, self).form_valid(form)
            #self.object contains the saved object
#             if self.object.related_contact != prev_contact:
#                 Note.objects.select_related().filter(related_call=self.object).update(related_contact=self.object.related_contact)
#             if self.object.related_work != prev_work:
#                 Note.objects.select_related().filter(related_call=self.object).update(related_work=self.object.related_work)
            return response

class CallCreate(AuthMixin, FilteringMixin, BaseMixin, CallMixin, utils.CustomMessageMixin, CreateView):
    
    model = Call
    form_class = CallForm
    
    def get_initial(self):
        initials = super(CallCreate, self).get_initial()
        return self.get_filtered_initial(initials)
        

class CallUpdate(AuthMixin, FilteringMixin, BaseMixin, CallMixin, utils.CustomMessageMixin, UpdateView):        
    
    model = Call
    form_class = CallForm
    
    def get_context_data(self, **kwargs):
        context = super(CallUpdate, self).get_context_data(**kwargs)
        note_ajax_url = reverse('views.note.call', args=[str(self.object.id)])
        note_options = NoteList.datatable_call_options.copy()        
        context['note_datatable'] = get_datatable_structure(note_ajax_url, note_options, Note)
        return context
    
class CallDelete(AuthMixin, DeleteView):
    model = Call
    success_url = reverse_lazy('views.call')
    
#-------------------Documents--------------------#

class DocumentList(FilteringMixin, AuthMixin, DatatableView):
    template_name = "erp/document_list.html"
    model = Document
    datatable_options = {
        'columns': ['file',
                    'type',
                    'date',
                    ('Pratica', ['related_work__description']),                    
                    ('Contatto', ['related_contact__last_name', 'related_contact__first_name']),
                    ('Tag', ['tags__name'], 'get_tags'),
                    ('Info', 'updated_by__username', 'get_updated_by'),                  
                    ],
        'structure_template' : 'erp/table_structure.html'
    }
    
    datatable_contact_options = {
        'columns': ['file',
                    'type',
                    'date',
                    ('Pratica', ['related_work__description']),
                    ('Tag', ['tags__name'], 'get_tags'),
                    ('Info', 'updated_by__username', 'get_updated_by'),
                    ],
        'structure_template' : 'erp/table_structure_noSearch.html'
    }
    
    datatable_work_options = {
        'columns': ['file',
                    'type',
                    'date',  
                    ('Contatto', ['related_contact__last_name', 'related_contact__first_name']),
                    ('Tag', ['tags__name'], 'get_tags'),
                    ('Info', 'updated_by__username', 'get_updated_by'),
                    ],
        'structure_template' : 'erp/table_structure_noSearch.html'
    }    
    
    def get_tags(self, instance, *args, **kwargs):
        tags = [tag for tag in instance.tags.all().order_by('name')]
        return utils.bootstrap_tag_label(tags)
    
    def get_updated_by(self, instance, *args, **kwargs):
        return utils.bootstrap_user_label(instance.updated_by)
    
    def get_column_file_data(self, instance, *args, **kwargs):
        return link_to_model(instance, text=instance.filename())
    
    def get_column_type_data(self, instance, *args, **kwargs):
        return instance.get_type_display()
    
    def get_column_Contatto_data(self, instance, *args, **kwargs):
        if instance.related_contact:
            return link_to_model(instance.related_contact, text=instance.related_contact)
        else:
            return ''

    def get_column_Pratica_data(self, instance, *args, **kwargs):
        if instance.related_work is None:
            return ''
        else:
            return link_to_model(instance.related_work, text=instance.related_work)
        
    def get_column_date_data(self, instance, *args, **kwargs):
        #return instance.date.strftime(DATE_FORMAT)
        return (utils.bootstrap_date_since_field(instance.date), instance.date)
    
    def get_queryset(self):
        return self.get_filtered_queryset()
    
    def get_datatable_options(self):
        return self.get_filtered_datatable_options()
    
class DocumentMixin(object):
    
    success_message = 'Documento salvato correttamente'
    
class DocumentCreate(AuthMixin, FilteringMixin, BaseMixin, DocumentMixin, utils.CustomMessageMixin, CreateView):
    model = Document
    form_class = DocumentForm
    
    def get_initial(self):
        initials = super(DocumentCreate, self).get_initial()
        return self.get_filtered_initial(initials)
    
#     def get_form_kwargs(self, *args, **kwargs):
#         super(DocumentCreate, self).get_form_kwargs(hide=['related_work', 'related_contact'])

class DocumentUpdate(AuthMixin, FilteringMixin, BaseMixin, DocumentMixin, utils.CustomMessageMixin, UpdateView):
    
    model = Document
    form_class = DocumentForm

    
class DocumentDelete(AuthMixin, DeleteView):
    model = Document
    success_url = reverse_lazy('views.document')
   
#-------------------Work--------------------#

class WorkList(FilteringMixin, AuthMixin, DatatableView):
    template_name = "erp/work_list.html"
    model = Work
    datatable_options = {
        'columns': ['description',
                    ('Stato pratica', 'current_status__status'),
                    ('Data', 'current_status__date', 'get_current_status_data'),
                    ('Contatti', ['related_contact__last_name', 'related_contact__first_name'], 'get_related_contacts'),
                    ('Tag', ['tags__name'], 'get_tags'),
                    ('Info', 'updated_by__username', 'get_updated_by'),
                    ],
        'structure_template' : 'erp/table_structure.html'
    }
    
    datatable_contact_options = {
        'columns': ['description',
                    ('Stato pratica', 'current_status__status'),
                    ('Data', 'current_status__date', 'get_current_status_data'),
                    ('Tag', ['tags__name'], 'get_tags'),
                    ('Info', 'updated_by__username', 'get_updated_by'),
                    ],
        'structure_template' : 'erp/table_structure_noSearch.html'
    }
    
#     def preload_record_data(self, instance):
#         tags = [tag for tag in instance.tags.all().order_by('name')]
#         contacts = [contact for contact in instance.related_contact.all().order_by('last_name', 'first_name', 'company')]
#         return (tags, contacts, )
    
    def get_tags(self, instance, *args, **kwargs):
        tags = [tag for tag in instance.tags.all().order_by('name')]
        return utils.bootstrap_tag_label(tags)
    
    def get_updated_by(self, instance, *args, **kwargs):
        return utils.bootstrap_user_label(instance.updated_by)

    def get_column_description_data(self, instance, *args, **kwargs):
        return link_to_model(instance, text=instance.description)
    
    def get_column_Stato_pratica_data(self, instance, *args, **kwargs):
        if instance.current_status is None:
            return ''
        else:
            return (instance.current_status.get_status_display(), instance.current_status.status)
    
    def get_current_status_data(self, instance, *args, **kwargs):
        if instance.current_status:
            return (utils.bootstrap_date_since_field(instance.current_status.date), instance.current_status.date)
        else:
            return ''
    
    def get_related_contacts(self, instance, *args, **kwargs):
        contacts = [contact for contact in instance.related_contact.all().order_by('last_name', 'first_name', 'company')]
        if contacts:
            return u'<ul>{0}</ul>'.format(u''.join(['<li>' + link_to_model(contact) + '</li>' for contact in contacts]))
        else:
            return ''
    
    def get_queryset(self):
        return self.get_filtered_queryset()

    def get_datatable_options(self):
        return self.get_filtered_datatable_options()
        
class WorkMixin(object):
    
    success_message = 'Pratica salvata correttamente'
    fail_message = 'Impossibile salvare la pratica. Controlla i dati inseriti.'

    def get_success_message(self, cleaned_data):
        return self.success_message % cleaned_data
    
    def form_valid(self, form, workstatustransition_formset):
        self.object = form.save()
        old_status = self.object.current_status 
        workstatustransition_formset.instance = self.object
        workstatustransition_formset.save()
        transitions = self.object.workstatustransition_set.all()
        current_status = None
        if transitions:
            current_status = max(transitions, key=lambda x:(x.status, x.date))      
        if current_status != old_status:
            self.object.current_status = current_status
            self.object.save()
        response = HttpResponseRedirect(self.get_success_url())
        success_message = self.get_success_message(form.cleaned_data)
        if success_message:
            messages.success(self.request, success_message)
        return response
    
    def form_invalid(self, form, workstatustransition_formset):        
        response = self.render_to_response(
            self.get_context_data(form=form,
                                  workstatustransition_formset=workstatustransition_formset,
                                  ))
        messages.warning(self.request, self.fail_message)
        return response

class WorkCreate(AuthMixin, FilteringMixin, BaseMixin, WorkMixin, CreateView):
    model = Work
    form_class = WorkForm
        
    def get(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form(self.get_form_class())
        workstatustransition_formset = WorkStatusTransitionFormSet(prefix='workstatustransition')
        return self.render_to_response(
            self.get_context_data(form=form,
                                  workstatustransition_formset=workstatustransition_formset,
                                  ))        
        
    def post(self, request, *args, **kwargs):        
        self.object = None
        form = self.get_form(self.get_form_class())
        workstatustransition_formset = WorkStatusTransitionFormSet(self.request.POST, prefix='workstatustransition')
        if (form.is_valid() and workstatustransition_formset.is_valid()):
            return self.form_valid(form, workstatustransition_formset)
        else:
            return self.form_invalid(form, workstatustransition_formset)
        
    def get_initial(self):
        initials = super(WorkCreate, self).get_initial()
        if FILTER_PARAM in self.kwargs.keys():
            filter_by = self.kwargs[FILTER_PARAM]
            try:
                if filter_by == Contact.__name__:
                    instance = Contact.objects.get(id=self.kwargs['pk'])
                    initials['related_contact'] = [instance,]
            except:
                pass
        return initials

class WorkUpdate(AuthMixin, FilteringMixin, BaseMixin, WorkMixin, UpdateView):
    model = Work
    form_class = WorkForm
    
    def get_context_data(self, **kwargs):
        context = super(WorkUpdate, self).get_context_data(**kwargs)
        document_ajax_url = reverse('views.document.work', args=[str(self.object.id)])
        document_options = DocumentList.datatable_work_options.copy()        
        context['document_datatable'] = get_datatable_structure(document_ajax_url, document_options, Document)        
        call_ajax_url = reverse('views.call.work', args=[str(self.object.id)])
        call_options = CallList.datatable_work_options.copy()        
        context['call_datatable'] = get_datatable_structure(call_ajax_url, call_options, Call)
        note_ajax_url = reverse('views.note.work', args=[str(self.object.id)])
        note_options = NoteList.datatable_work_options.copy()        
        context['note_datatable'] = get_datatable_structure(note_ajax_url, note_options, Note)
        return context
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form(self.get_form_class())
        workstatustransition_formset = WorkStatusTransitionFormSet(instance=self.object, prefix='workstatustransition')
        return self.render_to_response(
            self.get_context_data(form=form,
                                  workstatustransition_formset=workstatustransition_formset,
                                  ))        
        
    def post(self, request, *args, **kwargs):        
        self.object = self.get_object()
        form = self.get_form(self.get_form_class())
        workstatustransition_formset = WorkStatusTransitionFormSet(self.request.POST, instance=self.object, prefix='workstatustransition')
        if (form.is_valid() and workstatustransition_formset.is_valid()):
            return self.form_valid(form, workstatustransition_formset)
        else:
            return self.form_invalid(form, workstatustransition_formset)
    
class WorkDelete(AuthMixin, DeleteView):
    model = Work
    success_url = reverse_lazy('views.work')

#-------------------Note--------------------#

class NoteList(FilteringMixin, AuthMixin, DatatableView):
    template_name = "erp/note_list.html"
    model = Note
    datatable_options = {
        'columns': ['title',
                    'text',
                    'updated',
                    ('Contatto', ['related_contact__last_name', 'related_contact__first_name']),
                    ('Chiamata', ['related_call__subject', 'related_call__type']),
                    ('Pratica', ['related_work__description']),
                    ('Tag', ['tags__name'], 'get_tags'),
                    ('Info', 'updated_by__username', 'get_updated_by'),
                    ],
        'structure_template' : 'erp/table_structure.html'
    }
    
    datatable_contact_options = {
        'columns': ['title',
                    'text',
                    'updated',
                    ('Chiamata', ['related_call__subject', 'related_call__type']),
                    ('Pratica', ['related_work__description']),
                    ('Tag', ['tags__name'], 'get_tags'),
                    ('Info', 'updated_by__username', 'get_updated_by'),
                    ],
        'structure_template' : 'erp/table_structure_noSearch.html'
    }
    
    datatable_work_options = {
        'columns': ['title',
                    'text',
                    'updated',
                    ('Contatto', ['related_contact__last_name', 'related_contact__first_name']),
                    ('Chiamata', ['related_call__subject', 'related_call__type']),
                    ('Tag', ['tags__name'], 'get_tags'),
                    ('Info', 'updated_by__username', 'get_updated_by'),
                    ],
        'structure_template' : 'erp/table_structure_noSearch.html'
    }
     
    datatable_call_options = {
        'columns': ['title',
                    'text',
                    'updated',
                    ('Contatto', ['related_contact__last_name', 'related_contact__first_name']),
                    ('Pratica', ['related_work__description']),
                    ('Tag', ['tags__name'], 'get_tags'),
                    ('Info', 'updated_by__username', 'get_updated_by'),
                    ],
        'structure_template' : 'erp/table_structure_noSearch.html'
    }
    
    def get_tags(self, instance, *args, **kwargs):
        tags = [tag for tag in instance.tags.all().order_by('name')]
        return utils.bootstrap_tag_label(tags)
    
    def get_updated_by(self, instance, *args, **kwargs):
        return utils.bootstrap_user_label(instance.updated_by)
    
    def get_column_title_data(self, instance, *args, **kwargs):
        return link_to_model(instance, text=instance.title)
    
    def get_column_text_data(self, instance, *args, **kwargs):
        if instance.text:
            if len(instance.text) > 77:
                return instance.text[:77] + '...'
            else:
                return instance.text
        else:
            return ''
    
    def get_column_updated_data(self, instance, *args, **kwargs):
        #return instance.updated.strftime(DATE_FORMAT)
        return (utils.bootstrap_date_since_field(instance.updated), instance.updated)

    def get_column_Contatto_data(self, instance, *args, **kwargs):
        if instance.related_contact is None:
            return ''
        else:
            return link_to_model(instance.related_contact, text=instance.related_contact)
         
    def get_column_Chiamata_data(self, instance, *args, **kwargs):
        if instance.related_call is None:
            return ''
        else:
            return link_to_model(instance.related_call, text=instance.related_call)
    
    def get_column_Pratica_data(self, instance, *args, **kwargs):
        if instance.related_work is None:
            return ''
        else:
            return link_to_model(instance.related_work, text=instance.related_work)
    
    def get_queryset(self):
        return self.get_filtered_queryset()
    
    def get_datatable_options(self):
        return self.get_filtered_datatable_options()

class NoteMixin(object):
    success_message = 'Nota salvata correttamente'

class NoteCreate(AuthMixin, FilteringMixin, BaseMixin, NoteMixin, utils.CustomMessageMixin, CreateView):
    model = Note
    form_class = NoteForm

    def get_initial(self):
        initials = super(NoteCreate, self).get_initial()
        return self.get_filtered_initial(initials)

class NoteUpdate(AuthMixin, FilteringMixin, BaseMixin, NoteMixin, utils.CustomMessageMixin, UpdateView):
    model = Note
    form_class = NoteForm
    
class NoteDelete(AuthMixin, DeleteView):
    model = Note
    success_url = reverse_lazy('views.note')
    
#-------------------SEARCH--------------------#

class CustomSearchView(SearchView):
    def build_form(self, form_kwargs=None):
        self.searchqueryset = SearchQuerySet().autocomplete(content=self.request.GET.get('q', ''))
        return super(CustomSearchView, self).build_form(form_kwargs)
