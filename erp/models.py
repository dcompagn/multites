# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.models.fields.related import OneToOneField
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
import os
from taggit.managers import TaggableManager
import datetime

from django.utils.translation import pgettext, ugettext_lazy as _, ugettext

class UserExtModel(models.Model):
    related_user = models.OneToOneField(User, primary_key=True)
    color = models.CharField(max_length=6, blank=True, verbose_name=_('colore'))

    def __unicode__(self):
        return self.color


class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True, verbose_name=_('creato il'))
    updated = models.DateTimeField(auto_now=True, verbose_name=_('modificato il'))
    created_by = models.ForeignKey(User, editable=False, related_name='%(app_label)s_%(class)s_related_created_by', verbose_name=_('creato da'))
    updated_by = models.ForeignKey(User, editable=False, related_name='%(app_label)s_%(class)s_related_updated_by', verbose_name=_('modificato da'))
    class Meta:
        abstract = True

class Prov(models.Model):
    
    prov = models.CharField(max_length=128, verbose_name=_('povincia'))
    prov_abb = models.CharField(max_length=4)
    
    class Meta:
        verbose_name = _('provincia')
        verbose_name_plural = _('provincia')
        
    def __unicode__(self):
        return ', '.join([self.prov, self.prov_abb])
    
class City(models.Model):
    
    name = models.CharField(max_length=128, verbose_name=_('comune'))
    zip_code = models.CharField(max_length=8, verbose_name=_('cap'))
    prov = models.ForeignKey(Prov, verbose_name=_('provincia'))
    
    class Meta:
        verbose_name = _('comune')
        verbose_name_plural = _('comuni')
        
    def __unicode__(self):
        return self.name + ', ' + self.zip_code
        
class Contact(BaseModel):

    first_name = models.CharField(verbose_name=_('nome'), max_length=128, blank=True)
    last_name = models.CharField(verbose_name=_('cognome'), max_length=128, blank=True)
    company = models.CharField(verbose_name=_('azienda'), max_length=128, blank=True)
    email = models.EmailField(verbose_name=_('email'), blank=True)
    address = models.CharField(verbose_name=_('indirizzo'), max_length=128, blank=True)
    related_prov = models.ForeignKey(Prov, verbose_name=_('provincia'), blank=True, null=True)
    related_city = models.ForeignKey(City, verbose_name=_('comune'), blank=True, null=True)
    alt_address = models.CharField(verbose_name=_('indirizzo alternativo'), max_length=128, blank=True)
    alt_related_prov = models.ForeignKey(Prov, related_name='alt_prov', verbose_name=_('provincia (Ind. alternativo)'), blank=True, null=True)
    alt_related_city = models.ForeignKey(City, related_name='alt_city', verbose_name=_('comune (Ind. alternativo)'), blank=True, null=True)
    tags = TaggableManager(blank=True)
    
    class Meta:
        verbose_name = _('contatto')
        verbose_name_plural = _('contatti')
    
    def clean(self):
        super(Contact,self).clean()
        if bool(self.first_name) != bool(self.last_name):
            raise ValidationError(_('I campi "Nome" e "Cognome" oppure "Azienda" devono essere compilati'), code='blank')        
        elif not self.last_name and not self.company:
            raise ValidationError(_('I campi "Nome" e "Cognome" oppure "Azienda" devono essere compilati'), code='blank')
        else:
            pass
        
    def get_absolute_url(self):
        return reverse('views.contact.update', args=[str(self.id)])
    
    def get_path(self):
        path = ''.join(self.last_name.replace('/','').split()) + '_' + ''.join(self.first_name.replace('/','').split())
        return path
    
    def __unicode__(self):
        if (self.last_name):
            return '{0} {1}'.format(unicode(self.last_name), unicode(self.first_name))
        else:
            return '{0}'.format(unicode(self.company))
    
class PhoneNumber(models.Model):
    
    T_PRINCIPALE = 'R'
    T_PERSONALE = 'P'    
    T_AZIENDALE = 'A'
    T_CELLULARE = 'C'
    T_FISSO = 'F'
    T_CASA = 'S'
    T_FAX = 'X'
    T_UFFICIO = 'U'
    T_ALTRO = 'A'
    
    PHONE_TYPES = (                   
        (T_PRINCIPALE, _('Principale')),        
        (T_PERSONALE, _('Personale')),
        (T_AZIENDALE, _('Aziendale')),
        (T_CELLULARE, _('Cellulare')),
        (T_FISSO, _('Fisso')),
        (T_CASA, _('Casa')),
        (T_FAX, _('Fax')),
        (T_UFFICIO, _('Ufficio')),
        (T_ALTRO, _('Altro')),                 
    )
    
    number = models.CharField(max_length=16, verbose_name=_('numero'))
    type = models.CharField(max_length=1, choices=PHONE_TYPES, default=T_PRINCIPALE, verbose_name='tipo')
    related_contact = models.ForeignKey(Contact)
    updated = models.DateTimeField(auto_now=True, verbose_name=_('modificato il'))
    
    class Meta:
        verbose_name = _('telefono')
        verbose_name_plural = _('telefoni')
        
    def get_absolute_url(self):
        return reverse('views.contact.update', args=[str(self.related_contact.id)])
            
    def __unicode__(self):
        return u'{0} ({1})'.format(self.number, self.related_contact)
    
class Work(BaseModel):
    
    description = models.CharField(max_length=256, verbose_name=_('descrizione'))
    current_status = OneToOneField('WorkStatusTransition', blank=True, null=True, editable=False, verbose_name=_('stato attuale'), on_delete=models.SET_NULL)
    related_contact = models.ManyToManyField(Contact, verbose_name=_('contatti'))
    tags = TaggableManager(blank=True)
    
    class Meta:
        verbose_name = _('pratica')
        verbose_name_plural = _('pratiche')
    
    def __unicode__(self):
        return self.description
    
    def get_absolute_url(self):
        return reverse('views.work.update', args=[str(self.id)])
    
#     def get_current_status(self):
#         return WorkStatusTransition.objects.filter(related_work=self).order_by('-status').first()
    
    def get_path(self):
        path = '_'.join(self.description.replace('/','').split())
        return path
    
class WorkStatusTransition(models.Model):
    
    S_RICEVUTO_CONTATTO = '010'
    S_ATTESA_DOCUMENT = '020'
    S_DOCUMENT_ARRIVATA = '030'
    S_IN_DECISIONE = '040'
    S_FARE_PREVENTIVO = '050'
    S_PREVENTIVO_INVIATO = '060'
    S_RICONTATTO_UNO = '071'
    S_RICONTATTO_DUE = '072'
    S_RICONTATTO_TRE = '073'
    S_IN_VALUTAZIONE = '074'
    S_PRATICA_SCARTATA = '080'
    S_CONFERMATO_PREFENTIVO = '090'
    S_INTERVENTO_FISSATO = '100'
    S_INTERVENTO_ESEGUITO = '110'
    S_ELABORAZIONE_REPORT = '120'
    S_FATTURA_INVIATA = '130'
    S_SOLLECITO_PAGAMENTO = '131'
    S_PAGAMENTO_RICEVUTO = '140'
    S_REPORT_INVIATO = '150'
    S_ALTRO = '160'
    S_PARATICA_CHIUSA = '170'
    STATUS_TYPES = (
        (S_RICEVUTO_CONTATTO, '{0} ({1})'.format(unicode(S_RICEVUTO_CONTATTO), unicode(_('Ricevuto contatto')))), 
        (S_ATTESA_DOCUMENT, '{0} ({1})'.format(unicode(S_ATTESA_DOCUMENT), unicode(_('Attesa documentazione')))),
        (S_DOCUMENT_ARRIVATA, '{0} ({1})'.format(unicode(S_DOCUMENT_ARRIVATA), unicode(_('Documentazione arrivata')))), 
        (S_IN_DECISIONE, '{0} ({1})'.format(unicode(S_IN_DECISIONE), unicode(_('In fase di decisione')))),
        (S_FARE_PREVENTIVO, '{0} ({1})'.format(unicode(S_FARE_PREVENTIVO), unicode(_('Fare preventivo')))), 
        (S_PREVENTIVO_INVIATO, '{0} ({1})'.format(unicode(S_PREVENTIVO_INVIATO), unicode(_('Preventivo inviato')))),
        (S_RICONTATTO_UNO, '{0} ({1})'.format(unicode(S_RICONTATTO_UNO), unicode(_('Primo ricontatto')))),
        (S_RICONTATTO_DUE, '{0} ({1})'.format(unicode(S_RICONTATTO_DUE), unicode(_('Secondo ricontatto')))),
        (S_RICONTATTO_TRE, '{0} ({1})'.format(unicode(S_RICONTATTO_TRE), unicode(_('Terzo ricontatto')))),
        (S_IN_VALUTAZIONE, '{0} ({1})'.format(unicode(S_IN_VALUTAZIONE), unicode(_('Ancora in valutazione')))),
        (S_PRATICA_SCARTATA, '{0} ({1})'.format(unicode(S_PRATICA_SCARTATA), unicode(_('Pratica scartata')))),
        (S_CONFERMATO_PREFENTIVO, '{0} ({1})'.format(unicode(S_CONFERMATO_PREFENTIVO), unicode(_('Confermato preventivo')))),
        (S_INTERVENTO_FISSATO, '{0} ({1})'.format(unicode(S_INTERVENTO_FISSATO), unicode(_('Data intervento fissata')))),
        (S_INTERVENTO_ESEGUITO, '{0} ({1})'.format(unicode(S_INTERVENTO_ESEGUITO), unicode(_('Intervento eseguito')))),
        (S_ELABORAZIONE_REPORT, '{0} ({1})'.format(unicode(S_ELABORAZIONE_REPORT), unicode(_('Report in elaborazione')))),
        (S_FATTURA_INVIATA, '{0} ({1})'.format(unicode(S_FATTURA_INVIATA), unicode(_('Fattura inviata')))),
        (S_SOLLECITO_PAGAMENTO, '{0} ({1})'.format(unicode(S_SOLLECITO_PAGAMENTO), unicode(_('Sollecito pagamento')))),
        (S_PAGAMENTO_RICEVUTO, '{0} ({1})'.format(unicode(S_PAGAMENTO_RICEVUTO), unicode(_('Pagamento ricevuto')))),
        (S_REPORT_INVIATO, '{0} ({1})'.format(unicode(S_REPORT_INVIATO), unicode(_('Report inviato')))),
        (S_ALTRO, '{0} ({1})'.format(unicode(S_ALTRO), unicode(_('Altro')))),
        (S_PARATICA_CHIUSA, '{0} ({1})'.format(unicode(S_PARATICA_CHIUSA), unicode(_('Pratica chiusa')))),
    )
    
    related_work = models.ForeignKey(Work)
    status = models.CharField(max_length=3, choices=STATUS_TYPES, verbose_name=_('stato'))
    date = models.DateField(default=datetime.date.today, verbose_name=_('data'))
    note = models.TextField(blank=True, verbose_name=_('note'))
    updated = models.DateTimeField(auto_now=True, verbose_name=_('modificato il'))
    
    class Meta:
        verbose_name = _('stato')
        verbose_name_plural = _('stati')
        ordering = ['-status', '-date']
        
    def get_absolute_url(self):
        return reverse('views.work.update', args=[str(self.related_work.id)])
        
    def __unicode__(self):
        return u'{0} - {1}'.format(self.get_status_display(), self.related_work.description)

def get_upload_filename(instance, filename):
    if instance.type == Document.ESTIMATE:
        return 'documents/{0}/{1}'.format(unicode(instance.date.strftime("%Y/%m")), unicode(filename))
    else:
        return 'documents/{0}/{1}'.format(unicode(instance.date.strftime("%Y")), unicode(filename))

class Document(BaseModel):
    
    ESTIMATE = 'PV'
    INVOICE = 'FT'
    REPORT = 'RP'
    OTHER = 'AL'
    DOCUMENT_TYPES = (
        (ESTIMATE, '{0} ({1})'.format(unicode(ESTIMATE), unicode(_('Preventivo')))),
        (INVOICE, '{0} ({1})'.format(unicode(INVOICE), unicode(_('Fattura')))),
        (REPORT, '{0} ({1})'.format(unicode(REPORT), unicode(_('Report')))),
        (OTHER, '{0} ({1})'.format(unicode(OTHER), unicode(_('Altro')))),
    )
    
    file = models.FileField(upload_to=get_upload_filename, verbose_name=_('file'))
    type = models.CharField(max_length=2, choices=DOCUMENT_TYPES, default=ESTIMATE, verbose_name=_('tipo'))
    date = models.DateField(default=datetime.date.today, verbose_name=_('data'))
    description = models.CharField(max_length=256, blank=True, verbose_name=_('descrizione'))
    tags = TaggableManager(blank=True)
    related_contact = models.ForeignKey(Contact, blank=True, null=True, verbose_name=_('contatto'))
    related_work = models.ForeignKey(Work, blank=True, null=True, verbose_name=_('pratica'))    
    
    def filename(self):
        return os.path.basename(self.file.name)
    
    class Meta:
        verbose_name = _('documento')
        verbose_name_plural = _('documenti')
        
    def clean(self):
        super(Document,self).clean()
        if self.related_contact is None and self.related_work is None:
            raise ValidationError(_('Almeno un "Contatto" oppure una "Pratica" devono essere associati al documento'), code='blank')
        else:
            pass
    
    def __unicode__(self):
        return self.filename()
    
    def get_absolute_url(self):
        return reverse('views.document.update', args=[str(self.id)])

class Call(BaseModel):
    
    ENTRATA = 'E'
    USCITA = 'U'
    CALL_TYPES = (
        (ENTRATA, _('Entrata')),
        (USCITA, _('Uscita')),
    )
    
    related_contact = models.ForeignKey(Contact, verbose_name=_('contatto'))
    subject = models.CharField(verbose_name=_('oggetto'), max_length=256)
    type = models.CharField(max_length=1, choices=CALL_TYPES, default=ENTRATA, verbose_name=_('tipo'))
    date = models.DateField(default=datetime.date.today, verbose_name=_('data'))
    tags = TaggableManager(blank=True)
    related_work = models.ForeignKey(Work, blank=True, null=True, verbose_name=_('pratica'))
    
    class Meta:
        verbose_name = _('chiamata')
        verbose_name_plural = _('chiamate')
        
    def __unicode__(self):
        return self.subject
    
    def get_absolute_url(self):
        return reverse('views.call.update', args=[str(self.id)])
    
class Note(BaseModel):
    
    related_contact = models.ForeignKey(Contact, blank=True, null=True, verbose_name=_('contatto'))
    related_work = models.ForeignKey(Work, blank=True, null=True, verbose_name=_('pratica'))
    related_call = models.ForeignKey(Call, blank=True, null=True, verbose_name=_('chiamata'))
    title = models.CharField(max_length=256, verbose_name=_('titolo'))
    text = models.TextField(blank=True, verbose_name=_('testo'))
    tags = TaggableManager(blank=True)
    
    class Meta:
        verbose_name = _('nota')
        verbose_name_plural = _('note')
    
    def __unicode__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('views.note.update', args=[str(self.id)])
    