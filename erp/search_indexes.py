# -*- coding: utf-8 -*-

from haystack import indexes
from erp.models import Contact, PhoneNumber, Note, Call, Work, Document, WorkStatusTransition

#from haystack import signals

class ContactIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.NgramField(document=True, use_template=True)
    tags = indexes.MultiValueField()

    def prepare_tags(self, obj):
        return [tag.name for tag in obj.tags.all()]

    def get_model(self):
        return Contact
    
    def get_updated_field(self):
        """Specify which field is used to calculate the age of a Submission for re-indexing"""
        return 'updated'
    
    def index_queryset(self, using=None):        
        return self.get_model().objects.all()
    
class PhoneNumberIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.NgramField(document=True, use_template=True)
    
    def get_model(self):
        return PhoneNumber
    
    def get_updated_field(self):
        """Specify which field is used to calculate the age of a Submission for re-indexing"""
        return 'updated'
    
    def index_queryset(self, using=None):        
        return self.get_model().objects.all()
    
class NoteIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.NgramField(document=True, use_template=True)
    tags = indexes.MultiValueField()

    def prepare_tags(self, obj):
        return [tag.name for tag in obj.tags.all()]
    
    def get_model(self):
        return Note
    
    def get_updated_field(self):
        """Specify which field is used to calculate the age of a Submission for re-indexing"""
        return 'updated'
    
    def index_queryset(self, using=None):        
        return self.get_model().objects.all()
    
class WorkIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.NgramField(document=True, use_template=True)
    tags = indexes.MultiValueField()

    def prepare_tags(self, obj):
        return [tag.name for tag in obj.tags.all()]
    
    def get_model(self):
        return Work
    
    def get_updated_field(self):
        """Specify which field is used to calculate the age of a Submission for re-indexing"""
        return 'updated'
    
    def index_queryset(self, using=None):        
        return self.get_model().objects.all()
    
class CallIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.NgramField(document=True, use_template=True)
    tags = indexes.MultiValueField()

    def prepare_tags(self, obj):
        return [tag.name for tag in obj.tags.all()]
    
    def get_model(self):
        return Call
    
    def get_updated_field(self):
        """Specify which field is used to calculate the age of a Submission for re-indexing"""
        return 'updated'
    
    def index_queryset(self, using=None):        
        return self.get_model().objects.all()
    
class DocumentIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.NgramField(document=True, use_template=True)
    tags = indexes.MultiValueField()

    def prepare_tags(self, obj):
        return [tag.name for tag in obj.tags.all()]
    
    def get_model(self):
        return Document
    
    def get_updated_field(self):
        """Specify which field is used to calculate the age of a Submission for re-indexing"""
        return 'updated'
    
    def index_queryset(self, using=None):        
        return self.get_model().objects.all()
    
class WorkStatusTransitionIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.NgramField(document=True, use_template=True)
    
    def get_model(self):
        return WorkStatusTransition
    
    def get_updated_field(self):
        """Specify which field is used to calculate the age of a Submission for re-indexing"""
        return 'updated'
    
    def index_queryset(self, using=None):        
        return self.get_model().objects.all()